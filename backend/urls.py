from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from map import views
from utils import views as util_views
from rest_framework.authtoken import views as authtoken_views


router = routers.DefaultRouter()
router.register(r'violations', views.ViolationView, 'violations')
router.register(r'car-colors', views.CarColorView, 'car-colors')
router.register(r'car-brands', views.CarBrandView, 'car-brands')
router.register(r'utils', util_views.UtilsViewSet, 'utils')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api/auth-token/', authtoken_views.obtain_auth_token),
    path('', include('ui.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

