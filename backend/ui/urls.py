from django.urls import re_path

from . import views


urlpatterns = [
    re_path('^(?!(api)|(static)|(media)).*$', views.IndexView.as_view(), name='index'),
]
