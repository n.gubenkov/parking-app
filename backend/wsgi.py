import os
import sys

# Add the project directory to the Python path
sys.path.append('/home/frozmannik/parking-app')
sys.path.append('/home/frozmannik/parking-app/backend')

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')

application = get_wsgi_application()
