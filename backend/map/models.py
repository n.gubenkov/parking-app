from django.contrib.gis.db import models as gis_model
from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class CarColor(models.Model):
    color = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.color


class CarBrand(models.Model):
    brand = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.brand


class Violation(models.Model):
    coordinate = gis_model.PointField(srid=4326)
    # Up to 4 images
    image1 = models.ImageField(upload_to='images/', blank=True, null=True)
    image2 = models.ImageField(upload_to='images/', blank=True, null=True)
    image3 = models.ImageField(upload_to='images/', blank=True, null=True)
    image4 = models.ImageField(upload_to='images/', blank=True, null=True)

    plate_number = models.CharField(max_length=255, blank=False, null=True)
    brand = models.ForeignKey(CarBrand, on_delete=models.CASCADE)
    color = models.ForeignKey(CarColor, on_delete=models.CASCADE)
    comment = models.CharField(max_length=255, blank=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)


class ViolationHist(models.Model):
    coordinate = gis_model.PointField(srid=4326)
    created_at = models.DateTimeField(auto_now_add=True)


class Fine(models.Model):
    issued_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True)  # This tracks the creator
    violation = models.ForeignKey(
        Violation, on_delete=models.SET_NULL, null=True)
    comment = models.CharField(max_length=255, blank=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
