from django.contrib import admin
from .models import CarBrand, CarColor, Violation
from leaflet.admin import LeafletGeoAdmin

from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _


@admin.register(Violation)
class ViolationAdmin(LeafletGeoAdmin):
    list_display = ('id', 'brand', 'plate_number',
                    'color', 'comment', 'coordinate')
    readonly_fields = ('display_image1', 'display_image2',
                       'display_image3', 'display_image4')

    fieldsets = (
        (_('Violation Details'), {
            'fields': ('coordinate', 'display_image1', 'display_image2', 'display_image3', 'display_image4', 'brand', 'plate_number', 'color', 'comment'),
        }),
    )

    def display_image1(self, obj):
        return self._display_image(obj.image1)

    def display_image2(self, obj):
        return self._display_image(obj.image2)

    def display_image3(self, obj):
        return self._display_image(obj.image3)

    def display_image4(self, obj):
        return self._display_image(obj.image4)

    def _display_image(self, image_field):
        if image_field:
            return format_html('<img src="{}" style="max-width: 200px; max-height: 200px;" />', image_field.url)
        else:
            return _('No Image')

    display_image1.short_description = _('Image 1')
    display_image2.short_description = _('Image 2')
    display_image3.short_description = _('Image 3')
    display_image4.short_description = _('Image 4')


admin.site.register(CarBrand)
admin.site.register(CarColor)
