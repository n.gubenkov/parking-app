from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from .serialazers import get_simple_serializer
from .models import Violation, CarBrand, CarColor


class ViolationView(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = get_simple_serializer(Violation)
    queryset = Violation.objects.all()


class CarColorView(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = get_simple_serializer(CarColor)
    queryset = CarColor.objects.all().order_by('color')


class CarBrandView(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = get_simple_serializer(CarBrand)
    queryset = CarBrand.objects.all().order_by('brand')
