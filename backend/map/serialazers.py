from rest_framework import serializers


def get_simple_serializer(model_to_serialize):
    class serializer(serializers.ModelSerializer):
        class Meta:
            model = model_to_serialize
            fields = ('__all__')
    return serializer
