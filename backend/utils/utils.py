

def add_multiple_records(apps, app, model, data_to_insert):
    your_model = apps.get_model(app, model)
    for data in data_to_insert:
        your_model.objects.create(**data)
