import os
import random
import string

from django.core.files import File

from map.models import CarBrand, CarColor
from settings import BASE_DIR


def get_random_plate_number():
    letters = ''.join(random.choices(string.ascii_uppercase, k=3))

    # Generate 4 random digits
    digits = ''.join(random.choices(string.digits, k=3))

    # Combine letters and digits in the format ABC-123
    plate_number = f"{letters}{digits}"

    return plate_number


def get_random_car_brand():
    return CarBrand.objects.order_by('?').first()


def get_random_car_color():
    return CarColor.objects.order_by('?').first()


def get_random_parking_comment():
    # Lists of different elements to construct a parking comment
    adjectives = ["excellent", "bad", "average",
                  "convenient", "tight", "spacious", "crowded"]
    locations = ["near the entrance", "by the main gate", "at the back",
                 "close to the exit", "in the basement", "on the roof", "in the shaded area"]
    vehicles = ["car", "truck", "SUV", "motorbike", "van"]
    comments = [
        "It was easy to find a spot.",
        "Had to circle around a few times.",
        "Barely squeezed in!",
        "Lots of space left.",
        "Hard to park during peak hours.",
        "Perfect spot for my {}.",
        "The parking area was well lit.",
        "Not enough parking spots.",
        "No issues finding a spot.",
    ]

    # Randomly select elements
    adj = random.choice(adjectives)
    location = random.choice(locations)
    vehicle = random.choice(vehicles)
    comment = random.choice(comments).format(vehicle)

    # Construct the parking comment
    parking_comment = f"The parking was {adj} {location}. {comment}"

    return parking_comment


def get_random_coordinates():
    # coordinates are in format "SRID=4326;POINT (33.33937038074842 35.14770508596119)"
    # top left         35.17816 (Y), 33.32136  (x)             top right       35.17816, 33.39981
    # bottom left      35.09814, 33.32136                      bottom right    35.09814, 33.39981
    random_x_coordinate = random.uniform(35.09814, 35.17816)
    random_y_coordinate = random.uniform(33.32136, 33.39981)

    return f"SRID=4326;POINT ({random_y_coordinate} {random_x_coordinate})"


def get_random_image():
    # Use BASE_DIR for folder_path consistency
    folder_path = os.path.join('backend', 'utils', 'fill_example_data', 'img')

    # Check if the folder exists
    if not os.path.exists(folder_path):
        return None

    # Get a list of all files in the directory
    files = [f for f in os.listdir(folder_path) if os.path.isfile(
        os.path.join(folder_path, f))]

    # If there are no files, return None
    if not files:
        return None

    # Select a random file
    random_file = random.choice(files)
    random_file_path = os.path.join(folder_path, random_file)
    return File(open(random_file_path, 'rb'))


def create_random_violation():
    data = {
        'coordinate': get_random_coordinates(),
        'image1': get_random_image(),
        'plate_number': get_random_plate_number(),
        'brand': get_random_car_brand(),
        'color': get_random_car_color(),
        'comment': get_random_parking_comment()
    }

    return data
