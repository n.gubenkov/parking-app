from map.models import Violation
from utils.fill_example_data.utils import create_random_violation

NUMBER_OF_VIOLATIONS = 5


def create_violations():
    for _ in range(NUMBER_OF_VIOLATIONS):
        Violation.objects.create(**create_random_violation())


def delete_all_violations():
    Violation.objects.all().delete()
