from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from utils.fill_example_data.generate_example_data import create_violations, delete_all_violations
# Create your views here.


class UtilsViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    @action(methods=["post"], detail=False, url_path='generate-data')
    def generate_data(self, request):
        create_violations()
        return Response(status=status.HTTP_200_OK)

    @action(methods=["delete"], detail=False, url_path='delete-data')
    def delete_violations(self, request):
        delete_all_violations()
        return Response(status=status.HTTP_200_OK)
