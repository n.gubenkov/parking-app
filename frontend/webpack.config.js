const path = require("path");
const dotenv = require("dotenv");

// Load the same .env file
const envPath = path.resolve(__dirname, "../.env");
dotenv.config({ path: envPath });

const isDevMode = process.argv.includes("--mode=development");

process.env.BABEL_ENV = isDevMode ? "development" : "production";
process.env.NODE_ENV = isDevMode ? "development" : "production";

const configFactory = require("./config/webpack.config");

module.exports = configFactory(isDevMode ? "development" : "production");
