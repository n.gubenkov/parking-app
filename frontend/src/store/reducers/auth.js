import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    isLoggedIn: !!localStorage.getItem("token"),
    token: localStorage.getItem("token"),
    currentUser: {
        email: "mail@example.com",
        picture: null,
    },
};

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        loginUser: (state, { payload }) => {
            console.log("login user");
            console.log(payload);
            localStorage.setItem("token", payload);
            state.isLoggedIn = true;
            state.token = payload;
            axios.defaults.headers.common["Authorization"] = `Token ${payload}`;
            console.log("Updated state:", state);
            // I guess here should be logic updating axios
        },
        logoutUser: (state) => {
            localStorage.removeItem("token");
            state.currentUser = {};
            state.isLoggedIn = false;
            state.token = null;
            axios.defaults.headers.common["Authorization"] = null;
        },
        loadUser: (state, { payload }) => {
            if (!payload) {
                this.logoutUser(state);
                return;
            }

            console.log("CSRF TOKEN");

            state.currentUser = payload;
        },
    },
});

export const { loginUser, logoutUser, loadUser } = authSlice.actions;

export default authSlice.reducer;
