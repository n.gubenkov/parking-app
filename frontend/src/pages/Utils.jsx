import {
    MapContainer,
    TileLayer,
    Marker,
    Popup,
} from "react-leaflet";
import React, { useEffect, useState } from "react";
import { getCoordinate } from "../utils/utils";
import instance from "../utils/axios";
import "leaflet/dist/leaflet.css";
import {
    Button,
    Container,
} from "semantic-ui-react";
import "App.css";
import L from "leaflet";

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
    iconUrl: require("leaflet/dist/images/marker-icon.png"),
    shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
});

const Violations = () => {
    // center position of the map
    const [centerPosition] = useState({
        lat: 35.169183,
        lng: 33.360938,
    });
    const [markers, setMarkers] = useState([]);

    const handleBtnIssueFine = (violation_id) => {
        console.log("handleBtnIssueFine");
    };

    const generateData = () => {
        console.log("Generate random data pressed");
        instance.post(`/api/utils/generate-data/`).then((res) => {
            console.log("Inside generate data");
        });
    };

    const deleteData = () => {
        console.log("Delete all data pressed");
        instance.delete(`/api/utils/delete-data/`).then((res) => {
            console.log("Inside delete data");
        });
    };

    const handleBtnVehicleRemoved = (violation_id) => {
        console.log("handleBtnVehicleRemoved");
        instance.delete(`/api/violations/${violation_id}/`).then((res) => {
            console.log("Inside delete");
        });
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await instance.get("/api/violations/");
                console.log(response.data);
                setMarkers(response.data);
            } catch (error) {
                console.error("Error fetching data:", error);
            }
        };
        fetchData();
    }, []);

    return (
        <Container>
            Utils
            <MapContainer
                center={centerPosition}
                zoom={13}
                className="map-container"
            >
                <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
                {/* 33.35740758344463, 35.15668778850091 */}
                {/* Render markers if markers array is not null */}
                {markers !== null &&
                    markers.map((marker, index) => (
                        <Marker
                            key={index}
                            position={getCoordinate(marker.coordinate)}
                        >
                            <Popup>
                                <div>
                                    <img
                                        src={marker.image1}
                                        alt="Violation"
                                        style={{ maxWidth: "40%" }}
                                    />
                                    <p>Comment : {marker.comment}</p>
                                    <p>Plate Number: {marker.plate_number}</p>

                                    <Button
                                        style={{
                                            align: "center",
                                        }}
                                        color="red"
                                        onClick={() =>
                                            handleBtnIssueFine(marker.id)
                                        }
                                    >
                                        Issue a fine
                                    </Button>

                                    <Button
                                        style={{
                                            align: "center",
                                        }}
                                        color="green"
                                        onClick={() =>
                                            handleBtnVehicleRemoved(marker.id)
                                        }
                                    >
                                        Vehicle was removed
                                    </Button>
                                </div>
                            </Popup>
                        </Marker>
                    ))}
            </MapContainer>
            <Button
                style={{
                    align: "center",
                }}
                color="blue"
                onClick={generateData}
            >
                Generate random data
            </Button>
            <Button
                style={{
                    align: "center",
                }}
                color="red"
                onClick={deleteData}
            >
                Delete data
            </Button>
        </Container>
    );
};

export default Violations;
