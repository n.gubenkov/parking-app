import { MapContainer, TileLayer, Marker, useMapEvents } from "react-leaflet";
import React, { useEffect, useState } from "react";
import convertData from "../utils/utils";
import instance from "../utils/axios";
import "leaflet/dist/leaflet.css";
import {
    Button,
    Container,
    Header,
    Image,
    Dropdown,
    Form,
    FormInput,
} from "semantic-ui-react";
import "App.css";
import L from "leaflet";

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
    iconUrl: require("leaflet/dist/images/marker-icon.png"),
    shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
});

const style = {
    h1: {
        marginTop: "3em",
    },
    h2: {
        margin: "4em 0em 2em",
    },
    h3: {
        marginTop: "2em",
        padding: "2em 0em",
    },
    last: {
        marginBottom: "300px",
    },
};

const Report = () => {
    const initialFormState = {
        coordinate: null,
        plate_number: "",
        color: "",
        brand: "",
        comment: "",
    };

    const initialImages = {
        image1: null,
        image2: null,
        image3: null,
        image4: null,
    };

    const [markers, setMarkers] = useState(null);
    const [colorsOptions, setColorsOptions] = useState([]);
    const [brandsOptions, setBrandsOptions] = useState([]);
    const [form, setForm] = useState(initialFormState);

    const [images, setImages] = useState(initialImages);
    // form errors
    const [errors, setErrors] = useState({});

    // center position of the map
    const [centerPosition] = useState({
        lat: 35.169183,
        lng: 33.360938,
    });

    const handleMapClick = (e) => {
        const { lat, lng } = e.latlng;
        setMarkers([lat, lng]);
        setForm({
            ...form,
            coordinate: `SRID=4326;POINT (${lng} ${lat})`,
        });
    };

    const MapEventsHandler = ({ handleMapClick }) => {
        useMapEvents({
            click: (e) => handleMapClick(e),
        });
        return null;
    };

    const handleImageChange = (event, imageName) => {
        const file = event.target.files[0];
        setImages({ ...images, [imageName]: file });
    };

    const handleRemoveImage = (imageName) => {
        setImages({ ...images, [imageName]: null });
    };

    const renderImagePreview = (imageName) => {
        const image = images[imageName];
        return (
            <div className="image-preview-container">
                {image && (
                    <Image
                        src={URL.createObjectURL(image)}
                        alt={`Preview ${imageName}`}
                        className="image-preview"
                    />
                )}
                {image && (
                    <Button
                        color="red"
                        onClick={() => handleRemoveImage(imageName)}
                    >
                        Remove
                    </Button>
                )}
            </div>
        );
    };
    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value,
        });
    };

    const handleSelectChange = (e, data) => {
        setForm({
            ...form,
            [data.name]: data.value,
        });
    };

    const getColors = () => {
        instance.get("/api/car-colors/").then((res) => {
            setColorsOptions(convertData(res.data));
        });
    };

    const getBrands = () => {
        instance.get("/api/car-brands/").then((res) => {
            setBrandsOptions(convertData(res.data));
        });
    };

    const validateForm = () => {
        const newErrors = {};

        if (form.coordinate === null) {
            newErrors.coordinate = "Coordinate is require";
        }

        // Validate country
        if (form.plate_number === "") {
            newErrors.plate_number = "Plate number is require";
        }

        if (form.color === "") {
            newErrors.color = "Please select a color";
        }

        if (form.brand === "") {
            newErrors.brand = "Please select a brand";
        }

        if (form.comment === "") {
            newErrors.comment = "Comment is require";
        }

        setErrors(newErrors);

        // Return true if there are no errors, false otherwise
        return Object.keys(newErrors).length === 0;
    };

    const submitForm = (e) => {
        e.preventDefault();
        if (validateForm()) {
            const formDataToSend = new FormData();
            Object.entries(images).forEach(([key, value]) => {
                if (value) {
                    formDataToSend.append(key, value);
                }
            });
            Object.entries(form).forEach(([key, value]) => {
                formDataToSend.append(key, value);
            });

            instance.post("/api/violations/", formDataToSend);
            cleanForm();
        }
    };

    const cleanForm = () => {
        setForm(initialFormState);
        setMarkers(null);
        setImages(initialImages);
    };

    useEffect(() => {
        getColors();
    }, []);

    useEffect(() => {
        getBrands();
    }, []);

    useEffect(() => {}, [markers]);

    return (
        <>
            <Container>
                <Header as="h1" content="Report violation" textAlign="center" />

                <Form onSubmit={submitForm} enctype="multipart/form-data">
                    {errors.coordinate && (
                        <div className="ui pointing below red label">
                            {errors.coordinate}
                        </div>
                    )}

                    <MapContainer
                        center={centerPosition}
                        zoom={13}
                        className="map-container"
                        onClick={handleMapClick}
                    >
                        <TileLayer
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        />
                        <MapEventsHandler handleMapClick={handleMapClick} />

                        {/* Render markers if markers array is not null */}
                        {markers !== null && (
                            <Marker position={markers}></Marker>
                        )}
                    </MapContainer>

                    <Header
                        as="h3"
                        content="Image"
                        style={style.h1}
                        textAlign="center"
                    />
                    <Form.Field>
                        {[1, 2, 3, 4].map((index) => (
                            <Form.Field key={index}>
                                <label>{`Upload Image ${index}`}</label>
                                <input
                                    type="file"
                                    accept="image/*"
                                    onChange={(e) =>
                                        handleImageChange(e, `image${index}`)
                                    }
                                />
                                {renderImagePreview(`image${index}`)}
                            </Form.Field>
                        ))}
                    </Form.Field>

                    <Form.Input
                        error={
                            errors.plate_number && {
                                content: errors.plate_number,
                                pointing: "below",
                            }
                        }
                        fluid
                        name="plate_number"
                        label="Plate number"
                        onChange={handleChange}
                        value={form.plate_number}
                        placeholder="Plate number"
                        id="form-input-first-name"
                    />

                    <Form.Field>
                        <Dropdown
                            error={
                                errors.color && {
                                    content: errors.color,
                                    pointing: "below",
                                }
                            }
                            fluid
                            clearable
                            name="color"
                            options={colorsOptions}
                            onChange={(e, data) => handleSelectChange(e, data)}
                            value={form.color}
                            selection
                            search
                            placeholder={"Car color"}
                        />
                    </Form.Field>

                    <Form.Field>
                        <Dropdown
                            error={
                                errors.brand && {
                                    content: errors.brand,
                                    pointing: "below",
                                }
                            }
                            fluid
                            clearable
                            name="brand"
                            value={form.brand}
                            onChange={(e, data) => handleSelectChange(e, data)}
                            options={brandsOptions}
                            selection
                            search
                            placeholder={"Car brand"}
                        />
                    </Form.Field>

                    <Header
                        as="h3"
                        content="Comment"
                        style={style.h1}
                        textAlign="center"
                    />
                    <FormInput
                        error={
                            errors.comment && {
                                content: errors.comment,
                                pointing: "below",
                            }
                        }
                        fluid
                        name="comment"
                        onChange={handleChange}
                        value={form.comment}
                        placeholder="Comment"
                        id="form-input-comment"
                    />

                    <Button
                        style={{
                            align: "center",
                        }}
                        color="blue"
                        type="submit"
                    >
                        Submit
                    </Button>
                </Form>
            </Container>
        </>
    );
};

export default Report;
