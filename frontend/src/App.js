import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./modules/login/login.jsx";
import Report from "./pages/Report.jsx";
import Utils from "./pages/Utils.jsx";
import Violations from "./pages/Violation-Map";
import Protected from "./components/Protected";
import "leaflet/dist/leaflet.css";

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/login" element={<Login />} />
                <Route
                    path="/"
                    element={
                        <Protected>
                            <Report />
                        </Protected>
                    }
                />
                <Route
                    path="/violations"
                    element={
                        <Protected>
                            <Violations />
                        </Protected>
                    }
                />
                <Route
                    path="/utils"
                    element={
                        <Protected>
                            <Utils />
                        </Protected>
                    }
                />
            </Routes>
        </BrowserRouter>
    );
};

export default App;
