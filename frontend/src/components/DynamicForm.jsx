// src/components/DynamicForm.js
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Form, Button } from 'semantic-ui-react';

const DynamicForm = () => {
  const [fields, setFields] = useState({});
  const [formData, setFormData] = useState({});

  useEffect(() => {
    const fetchFields = async () => {
      try {
        const response = await axios.get('http://localhost:8000/api/violation-form-fields/');
        setFields(response.data);
      } catch (error) {
        console.error('Error fetching form fields:', error);
      }
    };

    fetchFields();
  }, []);

  const handleChange = (e, { name, value }) => {
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('http://localhost:8000/api/violations/', formData);
      console.log('Form submitted successfully:', response.data);
    } catch (error) {
      console.error('Error submitting form:', error);
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      {Object.keys(fields).map((field) => (
        <Form.Input
          key={field}
          label={field}
          name={field}
          value={formData[field] || ''}
          onChange={handleChange}
        />
      ))}
      <Button type="submit">Submit</Button>
    </Form>
  );
};

export default DynamicForm;