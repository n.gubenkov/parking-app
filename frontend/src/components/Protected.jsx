import React from "react";
import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";

import MenuBar from "./Menu";

function Protected({ children }) {
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

    if (!isLoggedIn) {
        return <Navigate to="/login" replace />;
    }

    return (
        <div style={{ paddingTop: "50px", paddingBottom: "20px" }}>
            <MenuBar />
            <div
                style={{
                    padding: "20px",
                    "@media (max-width: 768px)": { padding: "10px" },
                    "@media (min-width: 768px) and (max-width: 1024px)": {
                        padding: "15px",
                    },
                }}
            >
                {children}
            </div>
        </div>
    );
}
export default Protected;
