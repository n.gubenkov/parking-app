import SignOut from "../services/SignOut.js";
import { Container, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

function MenuBar() {
    return (
        <Menu fixed="top" inverted style={{ zIndex: 1000 }}>
            <Container>
                <Menu.Item header>Parking app</Menu.Item>
                <Menu.Item as={Link} to="/todo">
                    Todo
                </Menu.Item>
                <Menu.Item as={Link} to="/">
                    Report Violation
                </Menu.Item>
                <Menu.Item as={Link} to="/violations">
                    Violations Map
                </Menu.Item>
                <Menu.Item as={Link} to="/utils">
                    Utils
                </Menu.Item>
                <SignOut />
            </Container>
        </Menu>
    );
}

export default MenuBar;
