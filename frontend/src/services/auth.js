import axios from "axios";
import { fetchAPI } from "../utils/index";

export const loginByAuth = async (username, password) => {
    const resp = await fetchAPI("/api/auth-token/", {
        method: "POST",
        body: JSON.stringify({ username, password }),
    });
    const data = await resp.json();

    if (Math.floor(resp.status / 100) !== 2) {
        console.log(resp.status);
        throw new Error("Can not login with provided username and password");
    }

    return data.token;
};

export const getProfile = async () => {
    const resp = await axios.get("/api/users/me/");
    const user = await resp.json();

    if (Math.floor(resp.status / 100) !== 2) {
        throw new Error("Can not load profile data");
    }

    return {
        ID: user.id,
        createdAt: user.date_joined,
        email: user.email,
        isVerified: user.is_active,
        metadata: {},
        provider: "AUTH",
        updatedAt: user.last_login,
        username: user.username,
        permissions: user.user_permissions,
    };
};
