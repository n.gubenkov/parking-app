import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { logoutUser } from "store/reducers/auth";
import { Menu } from "semantic-ui-react";

const SignOut = (event) => {
    console.log("SING OUT LOGIC");
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleSignOut = (event) => {
        event.preventDefault();
        dispatch(logoutUser());
        navigate("/login");
    };

    return <Menu.Item onClick={handleSignOut}>Sign Out</Menu.Item>;
};

export default SignOut;
