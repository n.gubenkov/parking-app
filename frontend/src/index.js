import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "./index.css";
import { createRoot } from "react-dom/client";
import App from "./App";
import { Provider } from "react-redux";
import store from "./store/store";
import "semantic-ui-css/semantic.min.css";
import "leaflet/dist/leaflet.css";

const root = createRoot(document.getElementById("root"));

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>
);
