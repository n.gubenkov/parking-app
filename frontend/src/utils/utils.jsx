export default function convertData(inputData) {
    if (!inputData || inputData.length === 0) {
        return [];
    }

    const firstItem = inputData[0];
    const keyPropertyName = Object.keys(firstItem)[0];
    const textPropertyName = Object.keys(firstItem)[1];

    const convertedData = inputData.map((item) => {
        return {
            key: item[keyPropertyName],
            text:
                item[textPropertyName].charAt(0).toUpperCase() +
                item[textPropertyName].slice(1),
            value: item[keyPropertyName],
        };
    });

    return convertedData;
}

export function getCoordinate(coordinateObj) {
    // Extracting the coordinates from the string
    const matches = coordinateObj.match(/\(([^)]+)\)/);
    if (matches) {
        const coordinatesString = matches[1];
        const [lng, lat] = coordinatesString.split(" ").map(Number);
        return [lat, lng];
    } else {
        return [null, null];
    }
}
