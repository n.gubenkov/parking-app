import axios from "axios";
import store from "store/store";
import { logoutUser } from "store/reducers/auth";

const instance = axios.create();

// Set up default headers
instance.defaults.headers.common["Content-Type"] = "application/json";

// Add an interceptor to include the authentication token if available
instance.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem("token");
        if (token) {
            config.headers.Authorization = `Token ${token}`;
            instance.defaults.headers["WWW-Authenticate"] = `Token ${token}`;
            instance.defaults.headers.common["X-CSRFToken"] =
                document.body.dataset.csrfToken;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(
    (response) => response,
    (error) => {
        if (error.response.data.status === 401) {
            store.dispatch(logoutUser());
        }
        return Promise.reject(error);
    }
);

export default instance;
