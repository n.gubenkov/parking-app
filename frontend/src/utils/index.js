// require to do such request to be accepted by backend
export function fetchAPI(url, options) {
    options = options || {};
    let customHeaders = options.headers || {};
    let token = localStorage.getItem("token");
    let defaultHeaders = {
        "Content-Type": "application/json",
        "X-CSRFToken": document.body.dataset.csrfToken,
    };
    if (token) {
        defaultHeaders["Authorization"] = `Token ${token}`;
    }

    options.headers = { ...defaultHeaders, ...customHeaders };
    return fetch(url, options);
}
