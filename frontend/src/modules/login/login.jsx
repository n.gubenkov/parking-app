import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
    MDBContainer,
    MDBCol,
    MDBRow,
    MDBBtn,
    MDBInput,
    MDBCheckbox,
} from "mdb-react-ui-kit";
import * as AuthService from "../../services/auth";
import { loginUser } from "store/reducers/auth";
import { useFormik } from "formik";
import * as Yup from "yup";

function Login() {
    const [isAuthLoading, setAuthLoading] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const login = async (username, password) => {
        try {
            setAuthLoading(true);
            const token = await AuthService.loginByAuth(username, password);
            localStorage.setItem("authToken", token);
            dispatch(loginUser(token));
            navigate("/");
        } catch (error) {
            console.error("Login Error", error);
        } finally {
            setAuthLoading(false);
        }
    };

    const formik = useFormik({
        initialValues: {
            username: "",
            password: "",
        },
        validationSchema: Yup.object({
            username: Yup.string().required("Required"),
            password: Yup.string()
                .min(5, "Must be 5 characters or more")
                .max(30, "Must be 30 characters or less")
                .required("Required"),
        }),
        onSubmit: (values) => {
            login(values.username, values.password);
        },
    });

    return (
        <MDBContainer fluid className="p-3 my-5 login">
            <MDBRow>
                <MDBCol col="10" md="6">
                    <img
                        src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
                        className="img-fluid"
                        alt="Phone"
                    />
                </MDBCol>
                <form onSubmit={formik.handleSubmit} className="p-3 my-5 form">
                    <MDBCol col="8" md="6">
                        <MDBInput
                            wrapperClass="mb-4"
                            label="Login"
                            id="formControlLg"
                            type="text"
                            size="lg"
                            aria-label="Username"
                            {...formik.getFieldProps("username")}
                        />
                        {formik.touched.username && formik.errors.username ? (
                            <div className="text-danger">
                                {formik.errors.username}
                            </div>
                        ) : null}

                        <MDBInput
                            wrapperClass="mb-4"
                            label="Password"
                            id="formControlLg"
                            type="password"
                            size="lg"
                            aria-label="Password"
                            {...formik.getFieldProps("password")}
                        />
                        {formik.touched.password && formik.errors.password ? (
                            <div className="text-danger">
                                {formik.errors.password}
                            </div>
                        ) : null}

                        <div className="d-flex justify-content-between mx-4 mb-4">
                            <MDBCheckbox
                                name="flexCheck"
                                value=""
                                id="flexCheckDefault"
                                label="Remember me"
                            />
                            <a href="!#">Forgot password?</a>
                        </div>

                        <MDBBtn
                            className="mb-4 w-100"
                            style={{
                                outline: "none",
                                boxShadow: "none",
                                border: "none",
                            }}
                            size="lg"
                            type="submit"
                            disabled={isAuthLoading}
                        >
                            <span
                                style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                            >
                                {isAuthLoading ? (
                                    <div
                                        className="spinner-border spinner-border-sm"
                                        role="status"
                                        style={{
                                            marginRight: "0.5rem", // Space between spinner and text
                                        }}
                                    />
                                ) : null}
                                {isAuthLoading ? "Loading..." : "Sign in"}
                            </span>
                        </MDBBtn>
                    </MDBCol>
                </form>
            </MDBRow>
        </MDBContainer>
    );
}

export default Login;
