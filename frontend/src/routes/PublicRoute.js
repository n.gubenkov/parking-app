import React from 'react';
import {Navigate, Outlet } from 'react-router-dom';
import {useSelector} from 'react-redux';

const PublicRoute = ({children, ...rest}) => {
    const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
    const isAuthenticated = isLoggedIn || localStorage.getItem('token');

    return (
        <Outlet 
            {...rest}
            render={({location}) =>
                isAuthenticated ? (
                    <Navigate
                        to={{
                            pathname: '/',
                            state: {from: location}
                        }}
                    />
                ) : (
                    children
                )
            }
        />
    );
};

export default PublicRoute;
