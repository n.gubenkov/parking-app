import argparse
import os

from collections import defaultdict
from pathlib import Path


PROJECT_DIR = Path(__file__).resolve().parent.parent
TEMPLATES_DIR = Path(PROJECT_DIR, 'app_templates')


def render_app_files(destination_dir):
    print(f'destination {destination_dir}')
    for root, dirs, files in os.walk(TEMPLATES_DIR):
        relative_root = Path(root).relative_to(TEMPLATES_DIR)
        
        for filename in files:
            print(f'render :{filename}')
            os.makedirs(Path(destination_dir, relative_root), exist_ok=True)

            with open(Path(root, filename)) as f:
                template = f.read()

            try:
                rendered_content = template.format_map(defaultdict(str, **os.environ))
            except Exception:
                print(f'Can not render: {filename}')
                raise

            with open(Path(destination_dir, relative_root, filename), 'w') as f:
                f.write(rendered_content)
            print(f'render to {destination_dir}')
            print('finished')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Render app configs')
    parser.add_argument('--destination-dir', type=str, required=True)

    args = parser.parse_args()
    render_app_files(args.destination_dir)
