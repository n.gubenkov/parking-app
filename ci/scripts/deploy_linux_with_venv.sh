# Pipeline must stop after the first error
set -e

echo "test mkdir11"
mkdir mkdir11

echo "Render configurations"
python3 ci/scripts/render_app_files.py --destination-dir=

echo "test test"
mkdir test

echo "Prepare virtualenv"
python3 -m venv env
source env/bin/activate

echo "Installing packages"
pip install -r requirements.txt 


echo "Prepare frontend"
cd frontend/
npm install
npm run build

echo "Collect statics"
cd ../backend/
python3 manage.py collectstatic

echo "Running migrations"
python3 manage.py makemigrations


echo "Copy to deployment path"
cd ../.. 
cp -r parking-app /home/parking-app


echo "Copied"

echo "Restart supervisor"
sudo supervisorctl stop all
sudo supervisorctl reread
sudo supervisorctl reload

echo "Restart nginx"
sudo service nginx restart