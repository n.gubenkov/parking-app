import multiprocessing

# Binding Gunicorn to localhost on port 8000
bind = "127.0.0.1:8000"

# Number of worker processes (based on CPU count)
workers = multiprocessing.cpu_count() * 2 + 1

# Set working directory to the backend directory
directory = "/home/frozmannik/parking-app/backend"

# Ensure Python and Django can find the backend module
environment = {
    "DJANGO_SETTINGS_MODULE": "backend.settings",
    "PYTHONPATH": "/home/frozmannik/parking-app"
}

# Logging configuration (optional)
accesslog = "/home/frozmannik/parking-app/log/gunicorn_access.log"
errorlog = "/home/frozmannik/parking-app/log/gunicorn_error.log"
loglevel = "info"